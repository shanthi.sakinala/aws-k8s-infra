variable "aws_region" {
  description = "The aws region"
}

variable "instance_type" {
  description = "The aws EC2 instance type"
  default = "t2.micro"
}

variable "cluster_instance_type" {
  description = "The instance type o use for kubernetes cluster"
  default = "t2.small"
}

variable "keypair_name" {
  description = "The aws private key name"
}

variable "pod_network_cidr" {
  description = "The network address bloc for kubernetes pods"
}
variable "service_network_cidr" {
  description = "The network address bloc for kubernetes services"
}

variable ubuntu_user {
  description = "ansible user"
  default = "ubuntu"
}

variable "private_key" {
  description = "Aws private key file path"
}

variable "vpc_cidr" {
  description = "CIDR for the aws VPC"
  default = "10.0.0.0/16"
}

variable "availability_zones" {
  type = list
}

variable "public_subnet_cidrs" {
  description = "CIDR for the public subnets"
  type = list
}

variable "private_subnet_cidrs" {
  description = "CIDR for the private app subnets"
  type = list
}

variable "bastion_hostname" {
  description = "CIDR for the private app subnets"
}

variable "gitlab_runner_hostname" {
  description = "The name of the gitlab runner host"
}

variable "ansible_folder" {
  description = "The path of ansible projet"
}
variable "cloud_provider" {
  default = "aws"
}

variable "master_policy_file_names" {
  default = ["iam_ec2_elb_policy.json", "iam_external_dns_policy.json"]
}

variable "worker_policy_file_names" {
  default = ["iam_ec2_ecr_policy.json", "iam_external_dns_policy.json"]
}

variable "aws_user_name" {
  description = "The name your aws user account"
}

variable "domain_name" {
  description = "Application the domain name"
}

variable "aws_hostname_api" {
  description = "Aws api url to get ec2 hostname"
}

variable "ami_name" {
  description = "The name of the generated ami for k8s cluster"
}


