[![pipeline status](https://gitlab.com/rsihammou/aws-k8s-infra/badges/master/pipeline.svg)](https://gitlab.com/rsihammou/aws-k8s-infra/commits/master)

# Aperçu

Le projet consiste à créer une infrastructure pour gérer un cluster kubernetes sur aws.  
Sur ce cluster on pourra facilement déployé nos applications en se servant de gitlab-runner 
incorporé à notre infrastructure.  
L'infrastructure est versionnable, et son cycle de vie est gérée par un pipeline gitlab-ci.

# Schéma d'architecture

![k8s-infra-architecture](images/k8s-archi.png)

L'infrastructure est déployée sur deux zones de disponibilité, elle est composée des éléments suivants :
- Une instance ec2 bastion public(ayant une adresse ip public) jouant le rôle de pont et permet d'accéder aux instances privées via ssh.
- Un cluster kubernetes privé, composé de deux nœuds un worker et un autre à la fois master et worker.
- Une instance ec2 dédiée au gitlab-runner aussi non accessible au public, son but principal est d’assurer le déploiement de notre application sur le cluster kubernetes.
- Une route 53 pour gérer notre nom de domaine.
- Nginx-ingress controller pour exposer nos services vers l'extérieur de cluster.
- Cert-manager controller pour gérer le certificat tls. 
- External-dns controller pour créer des enregistrements dans la zone hébergée Route 53 afin de lier le domaine avec le load-balancer crée automatique par nginx-ingress controller.

# Prérequis

## Configuration de compte AWS

Pour déployer l'infrastructure, un compte Amazon AWS est nécessaire.  
Ajouter un utilisateur, et pour faire simple lui affecter tout les droits.

![USER MANAGEMENT](images/iam-create-user.png)

![USER MANAGEMENT](images/iam-create-user-2.png)

Récupérer la clé d'accès `ACCESS_KEY` et la clé d'accès secrète `SECRET_ACCESS_KEY`.

![USER MANAGEMENT](images/iam-create-user-3.png)

Au niveau de la console aws service EC2 créez votre Paire de clés (key pair).

![KEY PAIR](images/create-key-pair.png)

Remplissez le nom de la clé et choisissez le type pem, sauvegardez la par exemple dans le dossier 
`~/.ssh/` 

## Création du Compartiment S3

Pour Terraform nous utiliserons un backed de type S3. Utilisez le service AWS S3 pour créer un compartiment S3.

![COMPARTIMENT S3](images/s3-bucket.png)

Le fichier [terraform.tf](terraform.tf) doit contenir la configuration de backend S3.

```hcl
terraform {
  backend "s3" {
    bucket = "fr.alltech.terraform"
    key    = "terraform/state"
    region = "eu-west-3"
  }
}
```

# Infrastructure as code à l'aide de Terraform et Ansible

Terraform et Ansible vous permettent d'automatiser la création de votre infrastructure, 
le déploiement devient plus rapide et transparent, aucune intervention humaine n'est requise 
une fois le processus démarré.

On utilisera Terraform pour instancier les composants de notre infrastructure (EC2, sous-réseaux, groupe 
de sécurité …). 
Ansible viendra pour gérer la gestion de la configuration des différentes briques de l'infra.

## Terraform pour construire l'infrastructure

Les variables Terraform sont définis dans le fichier [terraform.tfvars](terraform.tfvars), mettez les 
à jour suivant en conformité avec votre environnement.

### Provider AWS

Vu que nous utilisons Amazon AWS comme fournisseur cloud, nous choisirons le provider AWS pour connecter 
Terraform à notre compte AWS.

```hcl
provider "aws" {
  version    = "~> 2.0"
  region     = var.aws_region
}
```

La variable `aws_region` est la région où vous voulez déployer votre infrastructure, pour mon cas c'est 
la région europe paris (`eu-west-3`).

### Network

La partie network est composée des éléments suivants :

#### 1-VPC (virtual private cloud)

- Block d'adressage `10.0.0.0/16`
- Service dns activé

Noté que nous opérons dans une infrastructure cloud privé, nous devons faire la distinction entre les ressources 
que Kubernetes doit gérer et celles qui ne doivent pas être modifiées.  
Dans AWS, Kubernetes utilise des balises pour faire cette distinction, il suffit alors de marquer les ressources 
par la balise ayant clé `kubernetes.io/cluster/[nom de cluster]` et la valeur `owned`.

```hcl
resource "aws_vpc" "main_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    Name = "terraform-k8s"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}
```

#### 2-Table de Routage

Nous ajoutons à notre VPC deux tables de routage :

- **Table de routage public :** utilisée pour un accès public à Internet, tout le trafic est redirigé vers 
l'internet gateway. 
- **Table de routage privée :** préserve un accès privé puisque tout le trafic est redirigé vers la NAT gateway.

#### 3-Subnets

Nous définissons pour chaque zone de disponibilité un sous-réseau public et un sous réseau-privé :

- **public_subnets :**

| Nom                    | Zone de disponibilité | Bloc d'adresse | Table de routage     | vpc           | balises                                                               |
|------------------------|-----------------------|:--------------:|----------------------|---------------|-----------------------------------------------------------------------|
| Terraform-k8s-public-1 | `eu-west-3a`          | `10.0.0.0/24`  | terraform-k8s-public | terraform-k8s | Name=terraform-k8s-public-1<br>kubernetes.io/cluster/kubernetes=owned |
| Terraform-k8s-public-2 | `eu-west-3b`          | `10.0.1.0/24`  | terraform-k8s-public | terraform-k8s | Name=terraform-k8s-public-2<br>kubernetes.io/cluster/kubernetes=owned |

- **private_subnets :**

| Nom                     | Zone de disponibilité | Bloc d'adresse  | Table de routage      | vpc           | balises                                                                |
|-------------------------|-----------------------|:---------------:|-----------------------|---------------|------------------------------------------------------------------------|
| Terraform-k8s-private-1 | `eu-west-3a`          | `10.0.100.0/24` | terraform-k8s-private | terraform-k8s | Name=terraform-k8s-private-1<br>kubernetes.io/cluster/kubernetes=owned |
| Terraform-k8s-private-2 | `eu-west-3b`          | `10.0.101.0/24` | terraform-k8s-private | terraform-k8s | Name=terraform-k8s-private-2<br>kubernetes.io/cluster/kubernetes=owned |

#### 4-Groupe de sécurité

À travers les règles de sécurité, nous avons la possibilité d'organiser le trafic au sein de notre réseau.

- **Bastion-Security-Group :**

**`Inbound`**

| Source      | Protocol | Port range | Description                                    |
|-------------|:--------:|:----------:|------------------------------------------------|
| `0.0.0.0/0` | SSH      | 22         | Autoriser l'accès SSH entrant pour tout.       |
| `0.0.0.0/0` | ICMP     | Tous       | Autoriser le protocole ICMP entrant pour tout. |

**`Outbound`**

| Source      | Protocol | Port range | Description                                           |
|-------------|:--------:|:----------:|-------------------------------------------------------|
| `0.0.0.0/0` | Tous     | Tous       | Autoriser la sortie vers l'ensemble des adresses IPv4 |

- **Runner-Security-Group :**

**`Inbound`**

| Source        | Protocol | Port range | Description                                                  | 
|---------------|:--------:|:----------:|--------------------------------------------------------------|
| Bastion       | SSH      | 22         | Autoriser l'accès SSH entrant pour l'instance bastion        |
| `10.0.0.0/16` | ICMP     | Tous       | Autoriser le protocole ICMP entrant depuis l'ensemble de VPC |

**`Outbound`**

| Source      | Protocol | Port range | Description                                           |
|-------------|:--------:|:----------:|-------------------------------------------------------|
| `0.0.0.0/0` | Tous     | Tous       | Autoriser la sortie vers l'ensemble des adresses IPv4 |

- **k8s-Security-Group :**

**`Inbound`**

| Source        | Protocol | Port range | Description                                                                             |
|---------------|:--------:|:----------:|-----------------------------------------------------------------------------------------|
| Bastion       | SSH      | 22         | Autoriser l'accès SSH entrant pour l'instance bastion.                                  |
| `10.0.0.0/16` | ICMP     | Tous       | Autoriser le protocole ICMP entrant depuis l'ensemble de VPC.                           |
| Runner        | TCP      | 6443       | Autoriser l'accès TC entrant pour pouvoir déployer les applications sur le cluster k8s. |
| Cluster k8s   | Tous     | Tous       | Autoriser la communication entre les instances de cluster k8s.                          |

**`Outbound`**

| Source      | Protocol | Port range | Description                                            |
|-------------|:--------:|:----------:|--------------------------------------------------------|
| `0.0.0.0/0` | Tous     | Tous       | Autoriser la sortie vers l'ensemble des adresses IPv4. |

Le groupe de sécurité cluster k8s doit porter la balise `kubernetes.io/cluster/[nom de cluster]` avec la valeur `owned`.

### Instances

#### 1-Bastion

Pour simplifier, on a rendu l'instance bastion accessible au public via SSH, ça sera plus judicieux de 
n'autoriser que les adresses IP des administrateurs.

| hostname | Type     | Image                                 | IP public | Sous-réseau            | Groupe de sécurité | Rôle IAM | stockage | balises                      |
|----------|----------|---------------------------------------|-----------|------------------------|--------------------|----------|----------|------------------------------|
| bastion  | t2.micro | ami-0d3f551818b21ed81<br>ubuntu-20.04 | oui       | terraform-k8s-public-1 | Bastion-sg         |          | 8Go      | Name = terraform-k8s-bastion |

Cette instance est attachée au public subnet de la zone de disponibilité `eu-west-3a`.

Nous utilisons l'image ubuntu v20.04 définie comme suivante :

```hcl
ata "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"]
}
```

#### 2-GitLab runner

| hostname      | Type     | Image                                 | IP public | Sous-réseau             | Groupe de sécurité | Rôle IAM | stockage | balises                     |
|---------------|----------|---------------------------------------|-----------|-------------------------|--------------------|----------|----------|-----------------------------|
| gitlab_runner | t2.micro | ami-0d3f551818b21ed81<br>ubuntu-20.04 | non       | terraform-k8s-private-1 | gitlab-runner-sg   |          | 8Go      | Name = terraform-k8s-runner |

L'instance dédiée au gitLab-runner est attachée au private subnet de la zone disponibilité `eu-west-3a`, 
cette instance ne possède pas d'adresse ip public, donc non accessible au public. 
Seul un accès en SSH est autorisé en provenance de l'instance bastion.

#### -Cluster k8s

Le cluster kubernetes est composé de deux instances :

- **Master**

| hostname           | Type     | Image                                 | IP public | Sous-réseau             | Groupe de sécurité | Rôle IAM            | stockage | balises                                                               |
|--------------------|----------|---------------------------------------|-----------|-------------------------|--------------------|---------------------|----------|-----------------------------------------------------------------------|
| DNS IPv4 privé (*) | t2.small | ami-0d3f551818b21ed81<br>ubuntu-20.04 | non       | terraform-k8s-private-1 | k8s-cluster-sg     | k8s_iam_master_role | 8Go      | Name = terraform-k8s-master<br>kubernetes.io/cluster/kubernetes=owned |

`*:` le cloud AWS utilise la valeur de DNS privée comme nom de nœud, 
alors il faut s'assurer que le hostname de l'instance correspond bien à cette valeur.

AWS met à notre disposition un service externe AWS exposé à l'adresse IP `169.254.169.254` qui permet de 
récupérer les meta-data de l'instance.
```sh
curl -s http://169.254.169.254/latest/meta-data/hostname
```
La valeur à récupérer est de la forme : `ip-[IP privé].[région].compute.internal`

Kubernetes aura besoin de communiquer avec plusieurs composants AWS via leurs APIs, nous aurons besoin de 
définir des autorisations ou rôle IAM.

Pour le nœud master, on lui attache le rôle [k8s_iam_master_role](roles.tf) :

```hcl
resource "aws_iam_role" "master_role" {
  name = "k8s_iam_master_role"
  assume_role_policy = file("${path.module}/policies/iam_ec2_role.json")
  tags = {
    Name = "terraform-k8s-master"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}
```
[am_ec2_role.json](policies/am_ec2_role.json)

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
```

Avec l'ensemble des autorisations décrites dans le fichier [am_ec2_elb_policy.json](policies/am_ec2_elb_policy.json)

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "autoscaling:DescribeAutoScalingGroups",
        "autoscaling:DescribeLaunchConfigurations",
        "autoscaling:DescribeTags",
        "ec2:DescribeInstances",
        "ec2:DescribeRegions",
        "ec2:DescribeRouteTables",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeSubnets",
        "ec2:DescribeVolumes",
        "ec2:CreateSecurityGroup",
        "ec2:CreateTags",
        "ec2:CreateVolume",
        "ec2:ModifyInstanceAttribute",
        "ec2:ModifyVolume",
        "ec2:AttachVolume",
        "ec2:AuthorizeSecurityGroupIngress",
        "ec2:CreateRoute",
        "ec2:DeleteRoute",
        "ec2:DeleteSecurityGroup",
        "ec2:DeleteVolume",
        "ec2:DetachVolume",
        "ec2:RevokeSecurityGroupIngress",
        "ec2:DescribeVpcs",
        "elasticloadbalancing:AddTags",
        "elasticloadbalancing:AttachLoadBalancerToSubnets",
        "elasticloadbalancing:ApplySecurityGroupsToLoadBalancer",
        "elasticloadbalancing:CreateLoadBalancer",
        "elasticloadbalancing:CreateLoadBalancerPolicy",
        "elasticloadbalancing:CreateLoadBalancerListeners",
        "elasticloadbalancing:ConfigureHealthCheck",
        "elasticloadbalancing:DeleteLoadBalancer",
        "elasticloadbalancing:DeleteLoadBalancerListeners",
        "elasticloadbalancing:DescribeLoadBalancers",
        "elasticloadbalancing:DescribeLoadBalancerAttributes",
        "elasticloadbalancing:DetachLoadBalancerFromSubnets",
        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
        "elasticloadbalancing:ModifyLoadBalancerAttributes",
        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
        "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
        "elasticloadbalancing:AddTags",
        "elasticloadbalancing:CreateListener",
        "elasticloadbalancing:CreateTargetGroup",
        "elasticloadbalancing:DeleteListener",
        "elasticloadbalancing:DeleteTargetGroup",
        "elasticloadbalancing:DescribeListeners",
        "elasticloadbalancing:DescribeLoadBalancerPolicies",
        "elasticloadbalancing:DescribeTargetGroups",
        "elasticloadbalancing:DescribeTargetHealth",
        "elasticloadbalancing:ModifyListener",
        "elasticloadbalancing:ModifyTargetGroup",
        "elasticloadbalancing:RegisterTargets",
        "elasticloadbalancing:DeregisterTargets",
        "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
        "iam:CreateServiceLinkedRole",
        "kms:DescribeKey"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
```
Une fois l'instance master est créée, un playbook Ansible est lancé automatiquement dans le but 
d'installer toutes les briques logicielles communes aux nœuds kubernetes.

```hcl
provisioner "local-exec" {
  command = <<EOT
    cd ${var.ansible_folder};
    ansible-playbook -i ${self.private_ip}, \
                     -u ${var.ubuntu_user} playbook-k8s-image.yml \
                     --tags "k8s"
  EOT
}
```

À la fin une image nommée `aws-k8s` sera créée et sauvegarder sur vitre compte AWS.

```hcl
resource  "aws_ami_from_instance" "k8s-ami" {
  name               = var.ami_name
  source_instance_id = aws_instance.master.id
  depends_on = [aws_instance.master]
  tags = {
    Name = var.ami_name
  }
}
```

- **Worker**

Le worker est basé sur l'image précédemment créée (`k8s_ami`).

| hostname           | Type     | Image   | IP public | Sous-réseau             | Groupe de sécurité | Rôle IAM            | stockage | balises                                                               |
|--------------------|----------|---------|-----------|-------------------------|--------------------|---------------------|----------|-----------------------------------------------------------------------|
| DNS IPv4 privé (*) | t2.small | k8s_ami | non       | terraform-k8s-private-2 | k8s-cluster-sg     | k8s_iam_worker_role | 8Go      | Name = terraform-k8s-worker<br>kubernetes.io/cluster/kubernetes=owned |

`*: curl -s http://169.254.169.254/latest/meta-data/hostname`

On attache aussi au worker le rôle [k8s_iam_worker_role](roles.tf) :

```hcl
resource "aws_iam_role" "worker_role" {
  name = "k8s_iam_worker_role"
  assume_role_policy = file("${path.module}/policies/iam_ec2_role.json")
  tags = {
    Name = "terraform-k8s-worker"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}
```
[am_ec2_role.json](policies/am_ec2_role.json)

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
```

Avec l'ensemble des autorisations décrites dans le fichier [iam_ec2_ecr_policy.json](policies/iam_ec2_ecr_policy.json)

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeInstances",
        "ec2:DescribeRegions",
        "ecr:GetAuthorizationToken",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetDownloadUrlForLayer",
        "ecr:GetRepositoryPolicy",
        "ecr:DescribeRepositories",
        "ecr:ListImages",
        "ecr:BatchGetImage"
      ],
      "Resource": "*"
    }
  ]
}
```

### Route 53

Pour utiliser notre domaine nous allons l'attaché à une route 53

```hcl
resource "aws_route53_zone" "rt53_zone" {
  name = var.domain_name
  tags = {
    Name = "terraform-k8s-rt53-zone"
  }
}
```

La variable `domaine_var` fait référence au domaine qui sera utilisé pour votre application.

### Addons Kubernetes

Tous les plugins additionnels à notre infrastructure seront installés via Ansible en utilisant 
les packages gérés par `helm`.

#### 1-Nginx-ingress

Nginx-ingress est un contrôleur d'entrée pour Kubernetes utilisant NGINX comme proxy inverse et équilibreur 
de charge.  
Pour exposer les services du cluster (déployé dans un cloud AWS), le Service ingress-nginx :

- Déploie un serveur Nginx dans le cluster.
- Crée un Load Balancer AWS `classic` en frontal du serveur Nginx créé. 

Nous avons déjà associé au profil des instances EC2 du cluster les autorisations nécessaires 
(lister/lire/écrire) sur les ressources `elasticloadbalancing`, ce qui permettra au contrôleur ingress-nginx 
de gérer un Load Balancer AWS.

#### 2-External-dns

Le module external-dns permet de créer des enregistrements dans la zone hébergée Route 53 (DNS Amazon).  
Au déploiement de l’Ingress, le contrôleur external-dns crée automatiquement deux enregistrements 
DNS (dans la zone hébergée indiquée)

- Un enregistrement ALIAS vers l’adresse externe de l’Ingress.
- Un enregistrement TXT qui spécifie l’origine de l’enregistrement (le nom de l’Ingress).

Pour que le Service external-dns soit capable de gérer un Load Balancer AWS, nous avons associé au profil 
des instances EC2 du cluster les autorisations nécessaires sur les ressources route53.

[iam_external_dns_policy.json](policies/iam_external_dns_policy.json)

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "route53:GetChange",
      "Resource": "arn:aws:route53:::change/*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "route53:ChangeResourceRecordSets"
      ],
      "Resource": [
        "arn:aws:route53:::hostedzone/*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "route53:ListHostedZones",
        "route53:ListResourceRecordSets",
        "route53:ListHostedZonesByName"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
```

#### 3-Cert-manager

Le cert-manager est le module qui permet de créer à la volée vos certificats TLS.  
Le cert-manager a besoin de créer un enregistrement TXT de nom _acme-challenge.<YOUR_DOMAIN> valorisé avec 
un token fourni par Let’s Encrypt, et ça dans le but de vérifier via un challenge que le client est 
propriétaire du nom domaine.

Pour que cert-manager puisse effectuer une modification au niveau de la zone hébergée route53, 
il faut lui attribuer des autorisations :

[cert_manager_policy.json](policies/cert_manager_policy.json)

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "route53:GetChange",
      "Resource": "arn:aws:route53:::change/*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "route53:ChangeResourceRecordSets",
        "route53:ListResourceRecordSets"
      ],
      "Resource": "arn:aws:route53:::hostedzone/*"
    },
    {
      "Effect": "Allow",
      "Action": "route53:ListHostedZonesByName",
      "Resource": "*"
    }
  ]
}
```

##  Ansible pour orchestrer la gestion de la configuration

Le projet Ansible est structuré de la manière suivante :

![STRUCTURE PROJET ANSIBLE](images/ansible-projet.png)

On trouve les fichiers playbook :

- **plyabook-k8s-image.yml :** pour préparer l'image ami qui sera la base des nœuds kubernetes.
- **playbook-k8s.yml :** installation de cluster kubernetes.
- **Playbook-runner :** pour installer le gitlab-runner.

Les roles : 

- **role common :** qui englobe toutes les procédures d'installation commune telle que docker.
- **role gitlab-runner :** dédié à l'installation et la configuration de gitlab-runner.
- **role master :** pour mettre en place un nœud master k8s.
- **role worker :** pour mettre en place un nœud worker k8s.

Les fichiers templates :

- **init_conf.yml :** configuration à utiliser pour initialiser un nœud master k8s.
- **join_conf.yml :** configuration à utiliser pour joindre un nœud master k8s.

### Configuration SSH

Les instances gitlab-runner, k8s-master et k8s-worker ne sont pas accessible au public, il faut impérativement 
passer par l'instance bastion.  
Nous utiliserons un rebond SSH pour permettre à Ansible de communiquer avec ces instances privées.

```yaml
ansible_ssh_common_args: '-o ProxyCommand="ssh -W %h:%p -q ${user}@${bastion_ip}"
```

### Roles Ansible

#### 1-Common role

Englobe les taches communes de la configuration et d'installation :

- Installation et configuration de docker.
- Installation et configuration de kubeadm.

#### 2-GitLab-runner role

Le but principal de ce rôle est d'installer et configurer un gitlab-runner de type shell executor :

- Installation et configuration de docker via une dépendance au rôle common.
- Installer le gitlab-runner.
- Ajout de gitlab-runner user tau docker group.
- Enregistrer le runner auprès de [https://gitlab.com](https://gitlab.com)

#### 3-Master role

permet d'initialiser et configurer le nœud master k8s.

- Générer la configuration pour initialiser le nœud master
- Initialiser le nœud master en utilisant kubeadm
- Déplacer la configuration admin.conf vers le dossier `/home/ubuntu/.kube/config`
- Installer le plugin réseau calico.
- Autoriser les déploiements sur le master.
- Installer Helm

La configuration kubeadm doit mentionner l'utilisation de cloud privé AWS.

[init_conf.yml.j2](ansible/templates/init_conf.yml.j2)

```yaml
apiVersion: kubeadm.k8s.io/v1beta2
kind: ClusterConfiguration
networking:
  serviceSubnet: {{ service_network_cidr }}
  podSubnet: {{ pod_network_cidr }}
apiServer:
  extraArgs:
    cloud-provider: {{ cloud_provider }}
controllerManager:
  extraArgs:
    cloud-provider: {{ cloud_provider }}
---
apiVersion: kubeadm.k8s.io/v1beta2
kind: InitConfiguration
nodeRegistration:
  ignorePreflightErrors:
  - NumCPU
```

`cloud_provider =  aws`

Nous ignorons l'erreur liée au nombre de `CPU` obligatoire vu qu'on utilise des images de type small avec 1 `VCPU`.

```yaml
IgnorePreflightErrors:
- NumCPU
```

#### 4-Worker role

Permet d'initialiser et configurer le nœud worker k8s.

- Générer un jeton pour jointure le cluster k8s.
- Générer le fichier de configuration de jointure.
- Joindre le cluster en utilisant kubeadm.
- Installer nginx-ingress 
- Installer external-dns 
- Installer cert-manager

Voici le template de configuration utilisée pour générer le fichier de jointure.

[join_conf.yml.j2](ansible/templates/join_conf.yml.j2)

```yaml
apiVersion: kubeadm.k8s.io/v1beta2
kind: JoinConfiguration
discovery:
  bootstrapToken:
    token: {{ kubeadm_token }}
    apiServerEndpoint: {{ kubeadm_apiserver }}
    caCertHashes:
      - {{ kubeadm_hash }}
nodeRegistration:
  name: {{ node_hostname }}
  kubeletExtraArgs:
    cloud-provider: {{ cloud_provider }}
```

## CI-CD avec gitlab-ci

### présentation

Le fichier `.gitlab-ci.yml` contient la configuration de processus d'intégration continue, 
ce dernier est composé de la liste des jobs suivantes :

![PIPELINE_GITLAB_CI](images/pipeline-gitlab.png)

- **Validate :** permet de valider le projet Terraform et détecter des éventuelles erreurs de syntaxe 
- **Apply :** lance la création de l'infrastructure sur le cloud. 
- **Cleanup_apply :** activé dans le cas où le job apply est en erreur, il permet de supprimer tous les éléments générés. 
- **Destroy :** supprime l'infrastructure crée au préalable.

Le pipeline est basé sur l'image `registry.gitlab.com/rsihammou/aws-k8s-infra/terraform-ansible-awscli:latest` 
dont les packages suivant sont déjà installés :

- Terraform v0.13.5
- Ansible version latest
- jq version latest
- awc-cli v1.18.166
- Ansibe community.kubernetes plugin pour Helm

```dockerfile
FROM hashicorp/terraform:0.13.5
MAINTAINER Rabia SIHAMMOU <rabie.sihammou@gmail.com>
ENV AWSCLI_VERSION "1.18.166"
RUN apk add --update \
    python3 \
    py3-pip \
    bash \
    jq \
    ansible \
    && ansible-galaxy collection install community.kubernetes \
    && pip install awscli==$AWSCLI_VERSION \
    && apk --purge -v del py-pip \
    && rm -rf /var/cache/apk/*
```

### Variables gitLab-ci

Pour faire tourner le pipeline, il faut d'abord configurer quelques variables.

Rendez-vous à la pae dédiée au paramétrage de gitlab-ci (`settiings -> CI-CD ->variables`), 
et ajoutes les variables suivantes :

![VARIABLES_GITLAB_CI](images/variables-gitlab-ci.png)

- **`AWS_ACCESS_KEY_ID` :** la valeur de vote AWS ACCESS KEY
- **`AWS_SECRET_ACCESS_KEY` :** la valeur de vote AWS SECRET ACCESS KEY
- **`PRIVATE_KEY` :** le contenu en base 64 de la clef privée AWS

### Pipeline gitlab-ci

#### 1-Validate job

```yaml
validate:
  stage: validate
  script:
    - terraform validate
    - terraform plan -var="private_key=$KEY_PATH" -out=$PLAN
  artifacts:
    paths:
      - $PLAN
```

Dans ce stade, le projet Terraform est validé et un plan d’exécution est généré 
et sauvegarder au niveau de fichier référencé par la variable PLAN.

#### 2-Apply job

```yaml
apply:
  stage: apply
  before_script:
    - export AWS_ACCESS_KEY_ID
    - export AWS_SECRET_ACCESS_KEY
    - mkdir ~/.ssh
    - chmod 700 ~/.ssh
    - which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )
    - ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -P ""
    - echo $PRIVATE_KEY | base64 -d > $KEY_PATH
    - chmod 400 $KEY_PATH
    - eval $(ssh-agent -s)
    - ssh-add ~/.ssh/id_rsa
    - ssh-add $KEY_PATH
    - |
      cat >> ~/.ssh/config <<EOL
      Host *
        StrictHostKeyChecking no
        ControlMaster auto
        ControlPersist 15m
      EOL
    - terraform init
  script:
    - terraform apply -input=false $PLAN
  dependencies:
    - validate
  when: manual
```

Ce job est exécuté manuellement, il permet de lancer la création automatique de l'infrastructure.  
Il faut d'abord valider le plan d'exécution généré au préalable, et si tout va bien vous pouvez lancer 
la création de l'infrastructure.

Pour permettre à Ansible de jouer le rebond `SSH`, il faut impérativement :
- Généré la clef public SSH de conteneur docker et l'ajouter au `SSH-AGENT`.
```sh
ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -P ""
ssh-add ~/.ssh/id_rsa
```
- Ajouter la clef privée AWS au `SSH-AGENT`.
```sh
echo $PRIVATE_KEY | base64 -d > $KEY_PATH
ssh-add $KEY_PATH
```
- Surcharger le fichier de configuration `SSH`.
```sh
cat >> ~/.ssh/config <<EOL
  Host *
    StrictHostKeyChecking no
    ControlMaster auto
    ControlPersist 15m
  EOL
```

#### 3-Destroy job

```yaml
destroy:
  stage: destroy
  script:
    - terraform destroy -auto-approve
  dependencies:
    - apply
  when: manual
```

Comme son nom le prédit, ce job a pour but de supprimer la globalité de l'infrastructure construite au préalable.
Une action manuelle est nécessaire pour son exécution, et elle dépend de l'étape apply.

Vu que nous avons ajouté des addons à notre cluster kubernetes, ces derniers modifient dynamiquement notre infrastructure :
- **`Nginx-ingress` :** ajoute dynamiquement un load-balancer pour permettre un accès externe à nos services.
- **`External-ds` :** configure dynamiquement les enregistrements DNS dans la zone hébergée Route 53.

Ces modifications interviennent en dehors de Terraform, ce que signifie que vous ne pouvez pas les supprimer avec 
terraform destroy. Alors, il faut passer par autre moyen, nous utilisons des scripts `aws-cli` pour se débarrasser de ces éléments 
en plus qui bloquent la suppression complète de l'infrastructure via Terraform.

D'abord, on supprime le load-balancer généré par `nginx-ingress` ainsi que toutes ses dépendances :

[delete-sg.sh](scripts/delete-sg.sh)

```sh
#!/bin/bash
region=$1;
bastion_id=$2;
vpc_id=$(aws ec2 describe-vpcs --region $region \
         --filters Name=tag:Name,Values="terraform-k8s" |
         jq --raw-output ".Vpcs[0].VpcId");
# Delete alb created by external-dns
for name in $(aws elb describe-load-balancers --region $region |
              jq --raw-output ".LoadBalancerDescriptions[].LoadBalancerName");
do
  aws elb delete-load-balancer --region $region --load-balancer-name $name;
done
# Terminate bastion ec2
aws ec2 terminate-instances --region $region --instance-ids $bastion_id;
# Wait for terraform to terminate other instances
for id in $(aws ec2 describe-instances --region $region --filters  \
            Name=vpc-id,Values="${vpc_id}" |
            jq --raw-output ".Reservations[].Instances[].InstanceId");
do
  while [ $(aws ec2 describe-instances --region $region --instance-ids $id \
            | jq --raw-output ".Reservations[].Instances[].State.Name") != "terminated" ];
  do
    echo "Wait until instances deletion to complete";
    sleep 5  
  done  
done
# Delete securityGroups
for id in $(aws ec2 describe-security-groups --region $region \
            --filters  Name=vpc-id,Values="${vpc_id}" \
            Name=tag:kubernetes.io/cluster/kubernetes,Values="owned" \
            | jq --raw-output ".SecurityGroups[].GroupId");
do
  aws ec2 delete-security-group --region $region --group-id $id;
done
```

On attache ce script à la phase de destruction de l'instance bastion par exemple.

```hcl
provisioner "local-exec" {
  when    = destroy
  command = "./scripts/delete-sg.sh ${self.tags.region} ${self.id}"
}
```

Ensuite on supprime les enregistrements dns ajoutés dynamiquement par external-dns.

[delete-route53-records.sh](scripts/delete-route53-records.sh)

```sh
#!/bin/bash
hosted_zone_id=$1;
aws route53 list-resource-record-sets \
  --hosted-zone-id $hosted_zone_id |
jq -c '.ResourceRecordSets[]' |
while read -r resourcerecordset; do
  type=$(jq -r '.Type' <<<"$resourcerecordset")
  name=$(jq -r '.Name' <<<"$resourcerecordset")
  if [ $type == "NS" -o $type == "SOA" ]; then
    echo "SKIPPING: $type $name"
  else
    change_id=$(aws route53 change-resource-record-sets \
      --hosted-zone-id $hosted_zone_id \
      --change-batch '{"Changes":[{"Action":"DELETE","ResourceRecordSet":
          '"$resourcerecordset"'
        }]}' \
      --output text \
      --query 'ChangeInfo.Id')
    echo "DELETING: $type $name $change_id"
  fi
done
```
Ce script est exécuté au moment de destruction de la route 53.

```hcl
provisioner "local-exec" {
  when    = destroy
  command = "./scripts/delete-route53-records.sh ${self.id}"
}
```

#### 4-Cleanup_apply job

Le but principal de ce job est de nettoyer tout ce qui a été généré à tort à cause d'une exécution non réussi de job apply.  
Ce job est exécuté automatiquement suite à l'échec de son prédécesseur.

```yaml
cleanup_apply:
  stage: destroy
  script:
    - terraform destroy -auto-approve
  dependencies:
    - apply
  when: on_failure
```

