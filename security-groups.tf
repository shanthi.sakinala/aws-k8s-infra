resource "aws_security_group" "k8s_sg" {
  description = "Allow incoming ssh connections."
  name = "k8s-cluster-sg"

  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
    cidr_blocks = [aws_vpc.main_vpc.cidr_block]
  }

  ingress {
    from_port = 6443
    protocol = "tcp"
    to_port = 6443
    security_groups = [aws_security_group.runner_sg.id]
  }

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    security_groups = [aws_security_group.bastion_sg.id]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-k8s-cluster"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

resource "aws_security_group_rule" "inside_k8s_sg_ingress"{
  description = "Allow connections inside k8s cluster"
  type = "ingress"
  from_port = 0
  to_port = 0
  protocol = -1
  source_security_group_id = aws_security_group.k8s_sg.id
  security_group_id = aws_security_group.k8s_sg.id

}

resource "aws_security_group" "runner_sg" {
  description = "Allow incoming ssh connections."
  name = "gitLab-runner-sg"

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    security_groups = [aws_security_group.bastion_sg.id]
  }

  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
    cidr_blocks = [aws_vpc.main_vpc.cidr_block]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-k8s-runner"
  }
}

resource "aws_security_group" "bastion_sg" {
  description = "Allow incoming ssh connections."
  name = "bastion-sg"
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "terraform-k8s-bastion"
  }
}


