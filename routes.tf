resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    Name = "terraform-k8s-public"
    "kubernetes.io/cluster/kubernetes" = "owned"
  }
}

resource "aws_route_table" "private_rt" {
  count = length(aws_subnet.private_subnets)
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = element(aws_nat_gateway.ngw.*.id, count.index)
  }
  tags = {
    Name = "terraform-k8s-private-${count.index + 1}"
  }
}

resource "aws_route_table_association" "public_rt_ass" {
  count = length(var.public_subnet_cidrs)
  route_table_id = aws_route_table.public_rt.id
  subnet_id = element(aws_subnet.public_subnets.*.id, count.index)
}

resource "aws_route_table_association" "private_rt_ass" {
  count = length(var.private_subnet_cidrs)
  route_table_id = element(aws_route_table.private_rt.*.id, count.index)
  subnet_id = element(aws_subnet.private_subnets.*.id, count.index)
}
